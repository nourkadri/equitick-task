<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mt5_deals', function (Blueprint $table) {

        $table->bigIncrements('Deal')->unsigned()->start_from(1);
		$table->bigInteger('Login')->unsigned()->default('0');
		$table->integer('Action')->unsigned()->default('0');
		$table->integer('Entry')->unsigned()->default('0');
		$table->timestamp('Time');
		$table->string('Symbol')->default('');
		$table->double('Price')->default('0');
        $table->double('Profit')->default('0');
		$table->bigInteger('Volume')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mt5_deals');
    }
};
