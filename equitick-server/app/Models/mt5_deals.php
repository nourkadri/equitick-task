<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
$mytime =  Carbon::now()->timestamp;
class mt5_deals extends Model
{
    
    use HasFactory;
    public $timestamps = true;
    const CREATED_AT = 'Time';
    const UPDATED_AT = null;
    protected $primaryKey = 'Deal';
    protected $fillable = [
        'Login', 'Action', 'Entry','Symbol','Price','Profit','Volume'
    ];
    public function login()
    {
        return $this->belongsTo(User::class,'id');
    }
    
 
}
