<?php

namespace App\Http\Controllers;
use App\Models\mt5_deals;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class Mt5DealsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store(Request $request)
    {
        $data = $request->only('Login', 'Action', 'Entry','Symbol','Price','Profit','Volume');
        $user=Auth::user();
        $Login=$user->id;
        if(count($request->all()) != 0) {
            $validator = Validator::make($data, [
                'Login' => 'required|integer',
                'Action' => 'required|integer',
                'Entry' => 'required|integer',
                'Symbol' => 'required|string',
                'Price' => 'required|numeric',
                'Profit' => 'required|numeric',
                'Volume' => 'required|integer',
            ],
            );
            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages()], 200);
            }
            $Login_req=$request->Login;
            if ((int)$Login !=  $Login_req){
                return response()->json(['error' => "error request! "], 200);
            }
            $Price=$request->Price;
    
            //get user balance and check if has enough balance or not
            $balance=Auth::user()->where('id',"=",$Login)->get('balance')->pluck('balance');
            if ($balance[0] < $Price){
                return response()->json(['error' => "You does not have enough balance! "], 200);
            }
    
    
            //balance deducation from the user
            Auth::user()->where('id',$Login)->update(['balance'=>$balance[0] - $Price]);
    
    
            //store the data
            mt5_deals::create($data);
    
            //return deals table whith new data
            return mt5_deals::where('Login','=',$Login)->simplePaginate(100);
        }
        //if no body get deals for user
        return mt5_deals::where('Login','=',$Login)->simplePaginate(100);
    }
    public function show(Request $request)
    {
        $user=Auth::user();
        $admin=$user->isAdmin;
        $Login=$request->Login;
        $Deal=$request->Deal;
        if($admin == true) {
            if(($request->has('Deal'))){
                return mt5_deals::where("Deal", "like", "%".$Deal.'%')->simplePaginate(9);
            }
            if(($request->has('Login'))){
                return mt5_deals::where("Login", "like", "%".$Login.'%')->simplePaginate(9);
            }
            if(($request->has('Deal')) && ($request->has('Login')) ){
                return mt5_deals::where(["Deal", "like", "%".$Deal.'%'],["Login", "like", "%".$Login.'%'])->simplePaginate(9);
            }
            return mt5_deals::simplePaginate(9);
        }
       
    }
    public function report(Request $request)
    {
        $users = User::count();    
        $deals = mt5_deals::count();    
        $admins = User::where("isAdmin", "=", 1)->count();
        return response()->json(['users' => $users,'deals' => $deals,'admins' => $admins], 200);
       
    }

}
