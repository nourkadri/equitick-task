# Hello, this task made by php(Laravel) and ReactJS
# to run server(Backend) you should have php-v8.1.9, mysql and composer on your system
## run this commends on terminal to run server
## cd equitick-server
## composer update
## php artisan serve
----------------------------------
# to run clinet(FrontEnd) you should have node on your system
## run this commends on terminal to run clinet
## cd equitick-clinet
## npm install
## npm start
