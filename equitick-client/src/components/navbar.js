import * as React from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import useToken from "../assest/useToken";
import LocalAtmIcon from "@mui/icons-material/LocalAtm";

export default function NavBar() {
 
  const [balance, setBalance] = React.useState();
  const [username, setUsername] = React.useState();
  const { authorisation } = useToken();


  const getBalance = async () => {
    const response = await fetch(
      "http://localhost:8000/api/balance",
      {
        method: "GET",
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Accept: "*/*",
          Authorization: `Bearer ${authorisation.token}`,
        },
      }
    );
    if (!response.ok) {
      throw new Error("Data coud not be fetched!");
    } else {
      return response.json();
    }
  };


  React.useEffect(() => {
    getBalance()
      .then((res) => {
        setBalance(res.balance);
        setUsername(res.username)
      })
      .catch((e) => {
        console.log(e.message);
      });
  });

  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand href="#">Equitick</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-5"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
          <div className="name"> Welcome: {username}</div>
           
          </Nav>
          <Form className="d-flex">
            <div className="balance">
              {balance}
              <LocalAtmIcon sx={{ fontSize: 70 }} />
            </div>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
