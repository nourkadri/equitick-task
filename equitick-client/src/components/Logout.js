import Button from "react-bootstrap/Button";
import useToken from "../assest/useToken";
import { useNavigate } from "react-router-dom";



export default function Logout(){
    const { authorisation } = useToken();
    let navigate = useNavigate();
    const fetchLogout = async () => {
        const response = await fetch(
          "https://equitick-task.herokuapp.com/api/logout",
          {
            method: "POST",
            headers: {
              "Content-type": "application/x-www-form-urlencoded",
              Accept: "*/*",
              Authorization: `Bearer ${authorisation.token}`,
            },
          }
        );
        if (!response.ok) {
          throw new Error("Data coud not be fetched!");
        } else {
          return response.json();
        }
      };
    const Logout = (e) => {
        e.preventDefault();
        window.localStorage.clear();
        navigate("/Login");
        fetchLogout()
          .then((res) => {})
          .catch((e) => {
            console.log(e.message);
          });
      };

    return(
    <Button  className="btn__logout" onClick={Logout}>Logout</Button>
    )
}