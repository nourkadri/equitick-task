import * as React from "react";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";

const currencies = [
  {
    value: "EURUSD",
    label: "EURUSD",
  },
  {
    value: "XAUUSD-",
    label: "XAUUSD-",
  },
  {
    value: "AD18DEC",
    label: "AD18DEC",
  },
  {
    value: "CL18NOV",
    label: "CL18NOV",
  },
  {
    value: "Brent_Z18",
    label: "Brent_Z18",
  },
  {
    value: "NGAS_X18",
    label: "NGAS_X18",
  },
  {
    value: "EURUSD-",
    label: "EURUSD-",
  },
];

export default function Currencies(props) {
  

  const handleChange = (event) => {
    props.setCurrency(event.target.value);
  };

  return (
    <TextField
      id="outlined-select-currency"
      select
      label="Select"
      value={props.currency}
      onChange={handleChange}
      helperText="Please select your currency"
    >
      {currencies.map((option) => (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </TextField>
  );
}
