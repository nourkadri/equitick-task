import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import * as React from "react";

export default function Table(props) {
  return (
    <div style={{ height: "65vh", width: "100%" }}>
      <DataGrid
        rows={props.rows}
        columns={props.columns}
        pageSize={9}
        getRowId={(rows) => rows.Deal}
        rowsPerPageOptions={[9]}
        //checkboxSelection
        disableSelectionOnClick
        components={{
          Toolbar: GridToolbar,
        }}
      />
    </div>
  );
}
