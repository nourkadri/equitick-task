import * as React from "react";
import { useNavigate } from "react-router-dom";
import useToken from "../assest/useToken";
import  Alert  from "./Alert";
export default function Auth() {
const [open, setOpen] = React.useState();
const [message, setMessage] = React.useState();
const [status, setStatus] = React.useState();
  let navigate = useNavigate();
  const [password, setPassword] = React.useState();
  const [email, setEmail] = React.useState();
  const { setToken } = useToken();
  const fetchData = async () => {
    const response = await fetch(
      "http://localhost:8000/api/login",
      {
        method: "POST",
        body: new URLSearchParams({
          email: email,
          password: password,
        }),
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Accept: "*/*",
        },
      }
    );
    if (!response) {
      throw new Error("Data coud not be fetched!");
    } else {
      return response.json();
    }
  };
 
  const Login = (e) => {
    e.preventDefault();
    fetchData()
      .then((res) => {
        setToken(res);
        if(res.hasOwnProperty("status") && res.status==="error"){
          setOpen(true);
          setMessage(res.message);
          return setStatus("error");
        }
        if (res.user.isAdmin===true){
          return navigate("/admin");
        }
        navigate("/dashboard");
      })
      .catch((e) => {
        console.log(e.message);
      });
    
  };
  const CreateAccount = (e) => {
    e.preventDefault();
   
        navigate("/register");
  };

  return (
    <div className="container">
    <Alert message={message} status={status} open={open} setOpen={setOpen}/>
      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card border-0 shadow rounded-3 my-5">
            <div className="card-body p-4 p-sm-5">
              <h5 className="card-title text-center mb-5 fw-light fs-5">
                Sign In
              </h5>
              <form onSubmit={Login}>
                <div className="form-floating mb-3">
                  <input
                    type="email"
                    className="form-control"
                    id="floatingInput"
                    placeholder="name@example.com"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <label htmlFor="floatingInput">Email address</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    type="password"
                    className="form-control"
                    id="floatingPassword"
                    placeholder="Password"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <label htmlFor="floatingPassword">Password</label>
                </div>
                <div className="d-grid">
                  <button
                    className="btn btn-primary btn-login text-uppercase fw-bold"
                    type="submit"
                  >
                    Sign in
                  </button>
                </div>
                <hr className="my-4" />
                <div className="d-grid mb-2">
                  <button
                    className="btn-secondary btn-create__account"
                   onClick={CreateAccount}
                  >
                    Create New Account
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}