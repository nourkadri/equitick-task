import * as React from "react";
import NavBar from "../../components/navbar";
import useToken from "../../assest/useToken";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router-dom";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import Table from "../../components/table";
import Loader from "../../components/Loader";
import Box from "@mui/material/Box";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import Alert from "../../components/Alert";
import Currencies from "../../components/Currencies";
import Logout from "../../components/Logout";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
export default function Dashboard() {
  const [openDialog, setOpenDialog] = React.useState(false);

  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const handleClose = () => {
    setOpenDialog(false);
  };
  const tokenString = localStorage.getItem("token");
  const isAd = JSON.parse(tokenString);
  let navigate = useNavigate();
  const first = React.useRef("");
  const second = React.useRef("");
  const third = React.useRef("");
  const [value, setValue] = React.useState(0);
  const [currency, setCurrency] = React.useState("EURUSD");
  const [open, setOpen] = React.useState();
  const [enable, setEnable] = React.useState(true);
  const [message, setMessage] = React.useState();
  const [status, setStatus] = React.useState();
  const { authorisation } = useToken();
  const [deals, seDeals] = React.useState(null);
  const [Action, setAction] = React.useState(0);
  const [Price, setPrice] = React.useState("");
  const [Entry, setEntry] = React.useState("");
  const [Profit, setProfit] = React.useState("");
  const [Volume, setVolume] = React.useState("");
  const login = localStorage.getItem("token");
  const Login = JSON.parse(login);
  const fetchData = async () => {
    const response = await fetch("http://localhost:8000/api/store", {
      method: "POST",
      headers: {
        "Content-type": "application/x-www-form-urlencoded",
        Authorization: `Bearer ${authorisation.token}`,
        Accept: "application/json",
        Connection: "keep-alive",
        "Accept-Encoding": "gzip, deflate, br",
      },
    });
    if (!response) {
      throw new Error("Data coud not be fetched!");
    } else {
      return response.json();
    }
  };

  const postDeal = async () => {
    const response = await fetch("http://localhost:8000/api/store", {
      method: "POST",
      body: new URLSearchParams({
        Login: Login.user.id,
        Action: Action,
        Entry: Entry,
        Symbol: currency,
        Price: Price,
        Profit: Profit,
        Volume: Volume,
      }),
      headers: {
        "Content-type": "application/x-www-form-urlencoded",
        Authorization: `Bearer ${authorisation.token}`,
        Accept: "application/json",
        Connection: "keep-alive",
        "Accept-Encoding": "gzip, deflate, br",
      },
    });
    if (!response) {
      throw new Error("Data coud not be fetched!");
    } else {
      return response.json();
    }
  };
  React.useEffect(() => {
    if (
      first.current.value === "" ||
      second.current.value === "" ||
      third.current.value === "" ||
      Entry === ""
    ) {
      return setEnable(true);
    }
    return setEnable(false);
  }, [Action, Entry, Price, Profit, Volume]);
  React.useEffect(() => {
    if (!authorisation) {
      return navigate("/Login");
    }
    let decodedToken = jwt_decode(authorisation.token);
    let currentDate = new Date();
    if (decodedToken.exp * 1000 < currentDate.getTime()) {
      return navigate("/Login");
    }
    if (isAd.user.isAdmin === 1) {
      return navigate("/admin");
    }

    fetchData()
      .then((res) => {
        seDeals(res.data);
      })
      .catch((e) => {
        console.log(e.message);
      });
  }, []);
  const Deal = () => {
    setEnable(true);
    postDeal()
      .then((res) => {
        if (res.hasOwnProperty("error")) {
          setOpen(true);
          setMessage(res.error);
          return setStatus("warning");
        }
        seDeals(res.data);
        setOpen(true);
        setMessage("Done!");
        setStatus("success");
        first.current.value = "";
        second.current.value = "";
        third.current.value = "";
      })
      .catch((e) => {
        console.log(e.message);
      });
    handleClose();
  };
  const columns = [
    { field: "Deal", headerName: "Deal", width: 150, identity: true },
    { field: "Login", headerName: "Login", width: 150 },
    {
      field: "Action",
      headerName: "Action",
      width: 300,
      renderCell: (params) => {
        return (
          <>
            {params.row.Action === 0 ? (
              <span style={{ color: "green" }}>
                Buy
                <ArrowDownwardIcon />
              </span>
            ) : null}
            {params.row.Action === 1 ? (
              <span style={{ color: "red" }}>
                Sell
                <ArrowUpwardIcon />
              </span>
            ) : null}
          </>
        );
      },
    },
    { field: "Time", headerName: "Time", width: 300 },
    { field: "Symbol", headerName: "Symbol", width: 300 },
    {
      field: "Entry",
      headerName: "Entry",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            {params.row.Entry === 0 ? (
              <span style={{ color: "green" }}>
                In
                <ArrowDownwardIcon />
              </span>
            ) : null}
            {params.row.Entry === 1 ? (
              <span style={{ color: "red" }}>
                Out
                <ArrowUpwardIcon />
              </span>
            ) : null}
          </>
        );
      },
    },
    {
      field: "Price",
      headerName: "Price",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <span style={{ fontWeight: "bold" }}>{params.row.Price}</span>
          </>
        );
      },
    },
    {
      field: "Profit",
      headerName: "Profit",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            {params.row.Profit > 0 ? (
              <span style={{ color: "green" }}>{params.row.Profit}</span>
            ) : null}
            {params.row.Profit <= 0 ? (
              <span style={{ color: "red" }}>{params.row.Profit}</span>
            ) : null}
          </>
        );
      },
    },
    { field: "Volume", headerName: "Volume", width: 150 },
  ];
  if (deals == null) {
    return <Loader />;
  }
  return (
    <div className="dashboard">
      <NavBar />
      <div>
        <div className="deal__btn">
          <Button variant="contained" onClick={handleClickOpen}>
            New Deal
          </Button>
        </div>
        <Dialog
          open={openDialog}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Box sx={{ width: 500 }}>
                <BottomNavigation
                  showLabels
                  value={value}
                  onChange={(event, newValue) => {
                    setValue(newValue);
                    setAction(newValue);
                  }}
                >
                  <BottomNavigationAction
                    value={0}
                    label="Buy"
                    icon={<ArrowDownwardIcon />}
                  />
                  <BottomNavigationAction
                    value={1}
                    label="Sell"
                    icon={<ArrowUpwardIcon />}
                  />
                </BottomNavigation>
                <FormControl className="radio_form">
                  <FormLabel className="radio_text">Entry</FormLabel>
                  <RadioGroup
                    className="radio_form"
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                    onChange={(e) => setEntry(e.target.value)}
                  >
                    <FormControlLabel
                      value="0"
                      control={<Radio />}
                      label="In"
                    />
                    <FormControlLabel
                      value="1"
                      control={<Radio />}
                      label="Out"
                    />
                  </RadioGroup>
                </FormControl>
              </Box>

              <Box
                component="form"
                sx={{
                  "& > :not(style)": { m: 1, width: "25ch" },
                }}
                noValidate
                autoComplete="off"
              >
                <Currencies currency={currency} setCurrency={setCurrency} />
                <TextField
                  inputRef={first}
                  id="outlined-basic"
                  label="Price"
                  variant="outlined"
                  type="number"
                  onChange={(e) => setPrice(e.target.value)}
                />
                <TextField
                  inputRef={second}
                  id="outlined-basic"
                  label="Profit"
                  type="number"
                  variant="outlined"
                  onChange={(e) => setProfit(e.target.value)}
                />
                <TextField
                  inputRef={third}
                  id="outlined-basic"
                  label="Volume"
                  type="number"
                  variant="outlined"
                  onChange={(e) => setVolume(e.target.value)}
                />
              </Box>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={Deal} disabled={enable} autoFocus>
              Deal
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <Table columns={columns} rows={deals} />
      <Logout />
      <Alert open={open} message={message} status={status} setOpen={setOpen} />
    </div>
  );
}
