import * as React from "react";
import { useNavigate } from "react-router-dom";
import useToken from "../../assest/useToken";
import Alert from "../../components/Alert";
export default function Register() {
  let navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const [password, setPassword] = React.useState();
  const [name, setName] = React.useState();
  const [passwordRe, setPasswordRe] = React.useState();
  const [email, setEmail] = React.useState();
  const [message, setMessage] = React.useState();
  const { setToken } = useToken();
  const fetchData = async () => {
    const response = await fetch(
      "http://localhost:8000/api/register",
      {
        method: "POST",
        body: new URLSearchParams({
          name: name,
          email: email,
          password: password,
        }),
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Accept: "application/json",
          Connection: "keep-alive",
          "Accept-Encoding":"gzip, deflate, br",
        },
      }
    );
    if (!response) {
      throw new Error("Data coud not be fetched!");
    } else {
      return response.json();
    }
  };
  const goToLogin= ()=>{
    navigate("/login");
  }
  const Register = (e) => {
    e.preventDefault();
    if (password !== passwordRe) {
      setMessage("passwords not match!");
      setOpen(true);
    }
    fetchData()
      .then((res) => {
        if (res.status === "success") {
          setToken(res);
          navigate("/");
        }
        if (res.errors.email) {
          setMessage(res.errors.email);
          setOpen(true);
        }
        if (res.errors.password) {
            setMessage(res.errors.password);
            setOpen(true);
          }
      })
      .catch((e) => {
        console.log(e.message);
      });
  };
  return (
    <section className="vh-100">
      <Alert message={message} open={open} setOpen={setOpen} status={"error"} />
      <div className="mask d-flex align-items-center h-100 gradient-custom-3">
        <div className="container h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-12 col-md-9 col-lg-7 col-xl-6">
              <div className="card" style={{ borderRadius: "15px" }}>
                <div className="card-body p-5">
                  <h2 className="text-uppercase text-center mb-5">
                    Create an account
                  </h2>
                  <form onSubmit={Register}>
                    <div className="form-outline mb-4">
                      <label className="form-label" htmlFor="form3Example1cg">
                        Your Name
                      </label>
                      <input
                        type="text"
                        id="form3Example1cg"
                        className="form-control form-control-lg"
                        onChange={(e) => setName(e.target.value)}
                      />
                    </div>
                    <div className="form-outline mb-4">
                      <label className="form-label" htmlFor="form3Example3cg">
                        Your Email
                      </label>
                      <input
                        type="email"
                        id="form3Example3cg"
                        className="form-control form-control-lg"
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </div>
                    <div className="form-outline mb-4">
                      <label className="form-label" htmlFor="form3Example4cg">
                        Password
                      </label>
                      <input
                        type="password"
                        id="form3Example4cg"
                        className="form-control form-control-lg"
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </div>
                    <div className="form-outline mb-4">
                      <label className="form-label" htmlFor="form3Example4cdg">
                        Repeat your password
                      </label>
                      <input
                        type="password"
                        id="form3Example4cdg"
                        className="form-control form-control-lg"
                        onChange={(e) => setPasswordRe(e.target.value)}
                      />
                    </div>
                    <div className="d-flex justify-content-center">
                      <button
                        type="submit"
                        className="btn-create__account"
                      >
                        Register
                      </button>
                    </div>
                    <p className="text-center text-muted mt-5 mb-0">
                      Have already an account?
                      <span onClick={goToLogin} className="fw-bold text-body">
                        <u>Login here</u>
                      </span>
                    </p>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
