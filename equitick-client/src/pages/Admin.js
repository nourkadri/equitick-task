import * as React from "react";
import useToken from "../assest/useToken";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router-dom";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import Table from "../components/table";
import Loader from "../components/Loader";
import Button from "@mui/material/Button";
import SkipNextIcon from "@mui/icons-material/SkipNext";
import SkipPreviousIcon from "@mui/icons-material/SkipPrevious";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Logout from "../components/Logout";
export default function Admin() {
  let navigate = useNavigate();
  const { authorisation } = useToken();
  const [deals, seDeals] = React.useState();
  const [Login, setLogin] = React.useState();
  const [Deal, setDeal] = React.useState();
  const [disableNext, setDisableNext] = React.useState(false);
  const [disablePrev, setDisablePrev] = React.useState(false);
  const [page, setPage] = React.useState(1);
  const [report, setReport] = React.useState([]);

  const fetchData = async () => {
    const params = new URLSearchParams({
      Deal: Deal,
      page: page,
      Login: Login,
    });
    params.forEach((value, key) => {
      if (value === "" || value === "undefined") {
        params.delete(key);
      }
    });
    console.log(params.toString());
    const response = await fetch("http://localhost:8000/api/show", {
      method: "POST",
      body: params,
      headers: {
        "Content-type": "application/x-www-form-urlencoded",
        Authorization: `Bearer ${authorisation.token}`,
        Accept: "application/json",
        Connection: "keep-alive",
        "Accept-Encoding": "gzip, deflate, br",
      },
    });
    if (!response) {
      throw new Error("Data coud not be fetched!");
    } else {
      return response.json();
    }
  };
  const getReports = async () => {
    const response = await fetch("http://localhost:8000/api/report", {
      method: "GET",
      headers: {
        "Content-type": "application/x-www-form-urlencoded",
        Accept: "*/*",
        Authorization: `Bearer ${authorisation.token}`,
      },
    });
    if (!response.ok) {
      throw new Error("Data coud not be fetched!");
    } else {
      return response.json();
    }
  };
  React.useEffect(() => {
    getReports()
      .then((res) => {
        setReport(res);
      })
      .catch((e) => {
        console.log(e.message);
      });
  }, []);
  React.useEffect(() => {
    setDisablePrev(true);
    setDisableNext(true);
    if (!authorisation) {
      return navigate("/Login");
    }
    let decodedToken = jwt_decode(authorisation.token);
    let currentDate = new Date();
    if (decodedToken.exp * 1000 < currentDate.getTime()) {
      return navigate("/Login");
    }
    fetchData()
      .then((res) => {
        if (res.prev_page_url != null && res.next_page_url != null) {
          setDisablePrev(false);
          setDisableNext(false);
        }
        if (res.prev_page_url === null && res.next_page_url === null) {
          setDisablePrev(true);
          setDisableNext(true);
        }
        if (res.next_page_url === null) {
          setDisableNext(true);
          setDisablePrev(false);
        }
        if (res.prev_page_url === null) {
          setDisablePrev(true);
          setDisableNext(false);
        }

        seDeals(res.data);
      })
      .catch((e) => {
        console.log(e.message);
      });
  }, [page]);

  const Search = () => {
    setPage(1);
    fetchData()
      .then((res) => {
        console.log(res);
        if (res.prev_page_url != null && res.next_page_url != null) {
          setDisablePrev(false);
          setDisableNext(false);
        }
        if (res.prev_page_url === null && res.next_page_url === null) {
          setDisablePrev(true);
          setDisableNext(true);
        }
        if (res.next_page_url === null) {
          setDisableNext(true);
          setDisablePrev(false);
        }
        if (res.prev_page_url === null) {
          setDisablePrev(true);
          setDisableNext(false);
        }

        seDeals(res.data);
      })
      .catch((e) => {
        console.log(e.message);
      });
  };
  const columns = [
    { field: "Deal", headerName: "Deal", width: 150, identity: true },
    { field: "Login", headerName: "Login", width: 150 },
    {
      field: "Action",
      headerName: "Action",
      width: 300,
      renderCell: (params) => {
        return (
          <>
            {params.row.Action === 0 ? (
              <span style={{ color: "green" }}>
                Buy
                <ArrowDownwardIcon />
              </span>
            ) : null}
            {params.row.Action === 1 ? (
              <span style={{ color: "red" }}>
                Sell
                <ArrowUpwardIcon />
              </span>
            ) : null}
          </>
        );
      },
    },
    { field: "Time", headerName: "Time", width: 300 },
    { field: "Symbol", headerName: "Symbol", width: 300 },
    {
      field: "Entry",
      headerName: "Entry",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            {params.row.Entry === 0 ? (
              <span style={{ color: "green" }}>
                In
                <ArrowDownwardIcon />
              </span>
            ) : null}
            {params.row.Entry === 1 ? (
              <span style={{ color: "red" }}>
                Out
                <ArrowUpwardIcon />
              </span>
            ) : null}
          </>
        );
      },
    },
    {
      field: "Price",
      headerName: "Price",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <span style={{ fontWeight: "bold" }}>{params.row.Price}</span>
          </>
        );
      },
    },
    {
      field: "Profit",
      headerName: "Profit",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            {params.row.Profit > 0 ? (
              <span style={{ color: "green" }}>{params.row.Profit}</span>
            ) : null}
            {params.row.Profit <= 0 ? (
              <span style={{ color: "red" }}>{params.row.Profit}</span>
            ) : null}
          </>
        );
      },
    },
    { field: "Volume", headerName: "Volume", width: 150 },
  ];
  if (deals == null) {
    return <Loader />;
  }
  return (
    <div className="dashboard">
      <div className="nav_admin">
        <h1>Admin Page</h1>
        <div className="main-section">
          <div className="dashbord">
            <div className="icon-section">
              <small>Deals</small>
              <p>{report.deals}</p>
            </div>
            <div className="detail-section"></div>
          </div>
          <div className="dashbord dashbord-orange">
            <div className="icon-section">
              <small>Users</small>
              <p>{report.users}</p>
            </div>
            <div className="detail-section"></div>
          </div>
          <div className="dashbord dashbord-red">
            <div className="icon-section">
              <small>Admins</small>
              <p>{report.admins}</p>
            </div>
            <div className="detail-section"></div>
          </div>
        </div>
      </div>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { mt: 2,ml:2, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          label="Deal"
          type="number"
          variant="outlined"
          onChange={(e) => setDeal(e.target.value)}
        />
        <TextField
          id="outlined-basic"
          label="Login"
          type="number"
          variant="outlined"
          onChange={(e) => setLogin(e.target.value)}
        />

        <Button
          variant="contained"
          color="success"
          onClick={Search}
          size="large"
          style={{height:"52px"}}
        >
          Search
        </Button>
      </Box>

      <Table columns={columns} rows={deals} />
      <Logout className="logout_btn" />
      <div className="next__prev">
        <Button
          disabled={disablePrev}
          onClick={(e) => setPage(Math.abs(page - 1))}
          size="large"
        >
          Prev <SkipPreviousIcon />
        </Button>
        <Button
          disabled={disableNext}
          onClick={(e) => setPage(page + 1)}
          size="large"
        >
          <SkipNextIcon /> Next
        </Button>
      </div>
    </div>
  );
}
